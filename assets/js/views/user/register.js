$(() => {
  $(document).ready(() => {
    $('form').submit((e) => {
      $('div.alert').hide();
      var password = $('input[type="password"]#inputPassword').val();
      var passwordR = $('input[type="password"]#inputPasswordR').val();
      if(password !== passwordR){
        $('div#pwmatch').show();
        e.preventDefault();
        return false;
      }
      return true;
    });
  });
  // Control form submit
});
