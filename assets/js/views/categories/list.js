$(() => {
  $(document).ready(() => {
    const picker = AColorPicker.createPicker({
      attachTo: 'div.colorPicker',
      showHSL: false,
      showRGB: false
    }).onchange = (picker) => {
      $('input#color').val(picker.color);
    };
    // Load dataTable
    $('#dataTables-categories').DataTable({
      responsive: true,
      order: [[0, "asc"]],
      pageLength: 25,
      stateSave: true,
      columnDefs: [{
        "targets": 3,
        "orderable": false,
        width: "20%"
      }]
    });
    // End Load DataTable
  });
});
