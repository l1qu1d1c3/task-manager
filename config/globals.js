/**
 * Global Variable Configuration
 * (sails.config.globals)
 *
 * Configure which global variables which will be exposed
 * automatically by Sails.
 *
 * For more information on any of these options, check out:
 * https://sailsjs.com/config/globals
 */

module.exports.globals = {
  _: require('@sailshq/lodash'),
  async: require('async'),
  models: true,
  sails: true,

  appName: "Task-Manager",
  appVersion: "0.1",

  bCryptSalt: 10,

  // Log in user automatically after register. Disable if we implement an email verification method
  loginAfterRegister: true
};
