/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */
module.exports = {

  datastores: {
    default: {
      adapter: 'sails-mongo',
      url: 'mongodb://localhost:27017/taskmanager',
      // ssl: true,
    },
  },

  models: {
    migrate: 'safe',
  },

  hookTimeout: 60000,

  // see https://github.com/balderdashy/sails-hook-email
  // IMPORTANT NOTE: DO NOT SET AUTH PARAMS, OTHERWISE IT WILL NOT WORK!
  email: {
      transporter: {
          url: "TODO"
      },
      from: "no-reply@task-manager.com",
      testMode: true,
      // the notification email can be different in each environment
      notifEmail: "alejandrofdez@me.com",
      mailingURL: "http://localhost:1337",
  },

  environmentDescription: "DEVELOPMENT",

  session: {

      /***************************************************************************
      *                                                                          *
      * Session secret is automatically generated when your new app is created   *
      * Replace at your own risk in production-- you will invalidate the cookies *
      * of your users, forcing them to log in again.                             *
      *                                                                          *
      ***************************************************************************/
      secret: 'dev682bcc8b3c5622a746d17e6cb82cc',


      /***************************************************************************
      *                                                                          *
      * Set the session cookie expire time The maxAge is set by milliseconds,    *
      * the example below is for 20min                                           *
      *                                                                          *
      ***************************************************************************/

       cookie: {
         maxAge: 1 * 20 * 60 * 1000
       }

  }

};
