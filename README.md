# task-manager

a [Sails v1](https://sailsjs.com) application

# Description
`This is a work in progress`
This site lets you create tasks + categories. It features a register/login system that allows users to sign up.

The level of database relations are:
Categories -> Tasks -> User

1 Category has N Tasks and 1 Task has 1 User(owner). 1 User has N tasks.

## Requirements
[`nodejs >= 8.0`](https://nodejs.org)
[`sailsjs`](http://sailsjs.com)
[`mongodb`](http://mongodb.com)

## Installation
Once the repository has been installed, run `npm install`. If you installed `sailsjs` globally, `cd` into the directory and run command `sails lift`

# What's next
* Category Creation from UI - Done
* Assigning color to categories - Done
* Back-end policies for restricting access and requiring permissions - Done
* Labelling tasks (with colors)
* Task details when clicked on them
* Task deletion from front-end
* User Panel
* User image uploading
* User registration with verification email

# Author
[Alejandro Fernandez](mailto:alejandrofdez@me.com)

# License
MIT license
