/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  dontUseObjectIds: true,
  attributes: {

    login: {
      type: "string",
      unique: true
    },

    name: {
      type: "string"
    },

    lastName: {
      type: "string"
    },

    email: {
      type: "string",
      unique: true
    },

    image: {
      type: "string"
    },

    tasks: {
      collection: "tasks",
      via: "owner"
    }
  }
};
