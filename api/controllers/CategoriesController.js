/**
 * CategoriesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  /**
   * @method getCategories
   * @desc gets categories
   * @param {object} req
   * @param {object} res
   */

  getCategories: async(req, res) => {
    if(!req) return res.badRequest();
    var categories = await CategoriesService.getCategories().catch((err) => {
      throw err;
    });
    return res.view('pages/categories/list', {categories: categories});
  },

  /**
   * @method createCategory
   * @desc creates a category
   * @param {object} req
   * @param {object} res
   */
  createCategory: async(req, res) => {
    if(!req) return res.badRequest();
    var data = req.allParams();
    var toSend = {};
    toSend.name = data.name,
    toSend.color = data.color+'4a';
    var createCategory = await CategoriesService.createCategory(toSend);
    return res.redirect('/categories');
  },

  /**
   * @method deleteCategory
   * @desc deletes a category
   * @param {object} req
   * @param {object} res
   */
  deleteCategory: async(req, res) => {
    if(!req)return res.badRequest();
    var id = req.param('id');
    if(!id) return res.redirect('/');
    await CategoriesService.deleteCategory(id);
    return res.redirect('/categories');
  }

};

