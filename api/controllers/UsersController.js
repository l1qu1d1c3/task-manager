/**
 * UsersController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var tasksController = require('./TasksController');
module.exports = {
  /**
   * @method loadRegisterView
   * @desc loads register view
   * @param {object}req
   * @param {object}res
   */
  loadRegisterView: async (req, res) => {
    if (!req.session.user) {
      return res.view("pages/user/register");
    }
    return res.redirect("/");
  },

  /**
   * @method loadLoginView
   * @desc loads login view
   * @param {object}req
   * @param {object}res
   */
  loadLoginView: async (req, res) => {
    if (!req.session.user || !req.session.authenticated) {
      return res.view("pages/user/login", { data: false });
    }
    var tasks = await TasksService.getTasks(req.session.user);
    // Redirect to another controller - this has to be improved...
    if(req.session.redirectURL){
      let redirectTo = req.session.redirectURL;
      delete req.session.redirectURL;
      return res.redirect(redirectTo);
    }
    return tasksController.getTasks(req, res);
    //return res.view("pages/tasks/list", {tasks: tasks});
  },

  /**
   * @method login
   * @desc logs user in
   * @param {object}req
   * @param {object}res
   */

  login: async (req, res) => {
    // Initialize error incase we need it
    let error = {
      msg: 'User or password incorrect'
    }
    var data = req.allParams();
    if (!data) return res.badRequest();
    // Filter we're sending to use to find user
    var filter = {};
    if (data.login.indexOf("@") !== -1) {
      filter = { email: data.login };
    } else {
      filter = { login: data.login };
    }

    var user = await UserService.findUser(filter).catch(err => {
      throw err;
    });

    if (!user){
      error.data = data;
      return res.view('pages/user/login', { error });
    }

    //Compare passwords
    var hashedPassword = await UserService.comparePasswords(
      user,
      data.password
    ).catch(err => {
      throw err;
    });
    if (!hashedPassword) return res.redirect('/');

    delete user.password;
    req.session.user = user;
    req.session.authenticated = true;

    return res.redirect("/");
  },
  /**
   * @method register
   * @desc registers user
   * @param {object}req
   * @param {object}res
   */

  register: async (req, res) => {
    var data = req.allParams();
    if (!data) return res.badRequest();
    // Back-end pw checks
    if (data.password !== data.passwordR) {
      return res.view("pages/user/register");
    }
    delete data.passwordR;
    data.password = await UserService.hashPassword(data.password);
    var userRegistered = await UserService.registerUser(data).catch(err => {
      throw err;
    });
    if(sails.config.globals.loginAfterRegister){
      req.session.user = userRegistered;
      req.session.authenticated = true;
    }
    return res.redirect('/');
  },

  /**
   * @method logout
   * @desc logs user out
   * @param {object}req
   * @param {object}res
   */

   logout: async(req,res) =>{
     req.session.destroy();
     return res.redirect('/');
   }
};
