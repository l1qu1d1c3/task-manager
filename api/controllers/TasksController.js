/**
 * TasksController
 *
 * @description :: Server-side logic for managing tasks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /**
   * @method getTasks
   * @desc calls service to get all tasks
   * @returns {array} of tasks
   */
  getTasks: async (req, res) => {
    if (!req.session.user || !req.session.authenticated) {
      return res.redirect('/login');
    }
    else {
      var allTasks = await TasksService.getTasks(req.session.user).catch((err) => {
        throw err;
      });
      var categories = await CategoriesService.getCategories().catch((err) => {
        throw err;
      });

      // We have categories, now we need to get those same tasks inside categories and their owner to merge it all up
      for(let i = 0; i < categories.length; i++){
        if(categories[i].tasks && categories[i].tasks.length > 0){
          for(let x = 0; x < categories[i].tasks.length; x++){
            var curTask = await TasksService.getTask(categories[i].tasks[x].id);
            categories[i].tasks[x] = curTask;
          }
        }
      }
      return res.view('pages/tasks/list', { tasks: (allTasks || []), categories: (categories || []) });
    }
  },

  /**
   * @method getTask
   * @desc calls service to get taks information
   * @returns {array} of tasks
   */
  getTask: async (req, res) => {
    if (!req) return res.badRequest();
    return res.ok();
  },

  /**
   * @method createTask
   * @desc creates a new task
   */
  createTask: async (req, res) => {
    if(!req)return res.badRequest();
    var data = req.allParams();
    data.owner = req.session.user.id;
    await TasksService.createTask(data);
    return res.redirect('/');
  }
};

