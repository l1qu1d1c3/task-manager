/**
 * SessionService
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */

module.exports = {
  /**
   * @method isLoggedIn
   * @desc checks if user is logged in
   * @param {object}user - session information
   * @returns {boolean}
   */

   isLoggedIn: async(user) => {
    //Functionality to be defined
   }
}
