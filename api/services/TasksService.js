/**
 * TasksService
 *
 * @description :: Server-side logic for managing Tasks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */

module.exports = {

  /**
   * @method getTasks
   * @desc gets user tasks
   * @param {object}user
   * @returns {array}
   */

  getTasks: async (user) => {
    if (!user) return false;
    var userTasks = await Users.find({ login: user.login }).populate('tasks').catch((err) => {
      throw err;
    });
    return userTasks.tasks;
  },

  /**
   * @method getTask
   * @desc gets task info and its relations
   * @param {string}id
   * @returns {object}
   */

  getTask: async (id) => {
    if (!id) return false;
    var taskData = await Tasks.findOne({id: id}).populate('owner').catch((err) => {
      throw err;
    });
    return taskData;
  },
  /**
   * @method createTask
   * @desc creates a task
   * @param {object}data
   * @returns {boolean}
   */
  createTask: async (data) => {
    if (!data) return false;
    var owner = data.owner.id;
    var category = data.category.id
    await Tasks.create(data).catch((err) => {
      throw err;
    });

    return true;
  },

  /**
   * @method deleteTask
   * @desc deletes a task
   * @param {object}task
   * @returns {boolean}
   */
  deleteTask: async(task) => {
    if(!task) return false;
    await Tasks.destroy({id: task.id}).catch((err) => {
      throw err;
    });
    return true;
  }
}
