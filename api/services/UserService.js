/**
 * UserService
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */
var bcrypt = require('bcrypt');
const hashSalt = sails.config.globals.bCryptSalt
module.exports = {
  /**
   * @method registerUser
   * @desc checks if user is logged in
   * @param {object}user - user data
   * @returns {boolean}
   */

  registerUser: async (user) => {
    if (!user) return false;
    var user = await Users.create(user).fetch().catch((err) => {
      throw err
    });
    return user;
  },

  /**
   * @method hashPassword
   * @desc method that hashes a password usiong bcrypt
   * @param {string} password user password
   * @returns {string} hashed password
   */
  hashPassword: async (password) => {
    var genSalt = bcrypt.genSaltSync(hashSalt);
    var hash = bcrypt.hashSync(password, genSalt);
    return hash;
  },

  /**
   * @method findUser
   * @desc finds user in database
   * @param {object}filter filter to find
   * @returns {object}
   */

  findUser: async (filter) => {
    if (!filter) return false;
    var user = await Users.findOne(filter).populate('tasks').catch((err) => {
      throw err;
    });
    return user
  },

  /**
   * @method comparePasswords
   * @desc compares password with one stored in database to check if we can log user in
   * @param {object}user user data
   * @param {string}password password input
   * @returns {boolean}
   */
  comparePasswords: async (user, password) => {
    if (!user || !password) return false;
    //Hash password provided to later compare it
    return bcrypt.compareSync(password, user.password);
  }
}
