/**
 * CategoriesService
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/services
 */

module.exports = {

  /**
   * @method getCategories
   * @desc gets categories
   * @returns {array}
   */

  getCategories: async () => {
    var categories = await Categories.find().populate('tasks').catch((err) => {
      throw err;
    });
    return categories;
  },

  /**
   * @method createCategory
   * @desc creates categories
   * @param {object}data
   * @returns {object}
   */

  createCategory: async (data) => {
    if (!data) return false;

    var newCategory = await Categories.create(data).fetch().catch((err) => {
      throw err;
    });
    return newCategory;
  },

  /**
   * @method getCategory
   * @desc gets one category
   * @param {string}id
   * @returns {object}
   */
  getCategory: async (id) => {
    if (!id) return false;

    var category = await Categories.findOne({ id: id }).populate('tasks').catch((err) => {
      throw err;
    });

    return category;
  },

  /**
   * @method deleteCategory
   * @desc deletes category
   * @param {string}id
   * @returns {boolean}
   */
  deleteCategory: async(id) => {
    if(!id) return false;
    var categoryAndTasks = await CategoriesService.getCategory(id);
    if(categoryAndTasks.tasks && categoryAndTasks.tasks.length > 0){
      categoryAndTasks.tasks.forEach(async (task) => {
        await TasksService.deleteTask(task);
      });
    }
    await Categories.destroy({id: id}).catch((err) => {
      throw err;
    });
    return true;
  }
};

